import Express from "express";

const app = Express.Router()

app.get('/:second', (req, res) => {
    const second = parseInt(req.params.second);
  
    setTimeout(() => {
        res.status(200).send(`Response with a ${second} second delay.`);
    }, second * 1000);
  });


export default app;