import express from 'express'
import { v4 as uuidv4 } from 'uuid';

const app = express.Router()


app.get('/', (req, res) => {
    const genrateUId = uuidv4();
    res.json({ uuid: genrateUId });
  });

export default app;