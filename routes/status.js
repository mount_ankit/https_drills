import Express from "express";

const app = Express.Router()

app.get('/:statusNum', (req, res) => {
    const statusCode = parseInt(req.params.statusNum);
    res.status(statusCode).send(`Response with status code: ${statusCode}`);
  });


export default app;