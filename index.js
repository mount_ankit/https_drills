import  express  from "express";
import html from './routes/html.js'
import json from './routes/json.js'
import uuid from './routes/uuid.js'
import status from './routes/status.js'
import delay from './routes/delay.js'


const app = express();

const port = 3000;



app.use('/html', html);
app.use('/json',json)
app.use('/uuid',uuid)
app.use('/status', status)
app.use('/delay',delay)


app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});
